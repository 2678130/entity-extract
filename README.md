# Simple city-entity extractor from text
## About
This library is for extracting the name of cities from input text.
It outputs a JSON object containing city information such as Name, Country, Lat/Long, Population.


## Usage

### Get cities
```javascript
const extractor = require('entity-extractor')

let string = "New York city is a beautiful city and Buenos Aires is also a beautiful city, is there Gatineau in the database?"

// returns an array JSON object containing cities in text
let output = extractor.getCities(string)

console.log(output)
```

### Output
```javascript
[
  {
    admin_code: 'NY',
    country: 'US',
    lat: '40.71427',
    long: '-74.00597',
    name: 'New York',
    population: '8175133'
  },
  {
    admin_code: '07',
    country: 'AR',
    lat: '-34.61315',
    long: '-58.37723',
    name: 'Buenos Aires',
    population: '13076300'
  },
  {
    admin_code: '10',
    country: 'CA',
    lat: '45.47723',
    long: '-75.70164',
    name: 'Gatineau',
    population: '242124'
  }
]
```